/*

import React, { Component } from 'react';

// rendering out alpha component
class PropsAndState extends Component {
    render() {
        return (
            <Alpha />
        )
    }
}

export default PropsAndState;

// Alpha class - stateful class component (there's state in alpha)
// When state is updated, 
// react always rerender the things that's updated from last render
class Alpha extends Component {
    // constructor() {
    //     super(props);

    //     state = {
    //         alphaValue: 'asdf'
    //     }
    // }
    // above is the same as below 
    state = {
        alphaValue: 'Data from Alpha!',
        counter: 1
    }

    // state should be treated as "immutable"
    // So when update counter, 
    // this.state.counter = this.state.counter + 1 is wrong
    // Instead, copy the counter, and use .setState()
    clickButton = () => {
        console.log("click");
        console.log(this.state.counter);

        let copyCounter = this.state.counter;
        copyCounter += 1;

        this.setState({
            counter: copyCounter
        })
        // above is the same as below
        // this.setState(previous => {counter: previous.counter + 1});
    }

    render() {
        return (
            <div>
                <button onClick={this.clickButton}>A button!</button>
                <Beta fromAlpha={this.state.alphaValue} />
            </div>
        )
    }
}

// Beta class
class Beta extends Component {
    state = {
        alphaValue: this.props.fromAlpha,
        betaValue: 'This is data from Beta!'
    }

    render() {
        return (
            <div>
                <p>This is Beta showing a prop sent down from Alpha: <b>{this.props.fromAlpha}</b></p>
                <p>This is Beta showing data from state: <b>{this.state.betaValue}</b></p>
            </div>
        )
    }
}

*/