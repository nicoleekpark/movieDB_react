// Configuration for TMDB
// Fetch configuration from 'api.themoviedb.org'

const API_URL = 'https://api.themoviedb.org/3/';
const API_KEY = '14b2ddd7728d37451f04cac12158f2ba';

// Images
// An image URL looks like this below:
// https://image.tmdb.org/t/p/w500/kqjL17yufvn9OVLyXYpvtyrFfak.jpg

const IMAGE_BASE_URL = 'http://image.tmdb.org/t/p/';
const BACKDROP_SIZE = 'w1280';
const POSTER_SIZE = 'w500';

export {
    API_URL,
    API_KEY,
    IMAGE_BASE_URL,
    BACKDROP_SIZE,
    POSTER_SIZE
}