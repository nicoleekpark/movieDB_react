import React, { Component } from 'react';
import { API_URL, API_KEY, IMAGE_BASE_URL, POSTER_SIZE, BACKDROP_SIZE } from '../../config';
import HeroImage from '../elements/HeroImage/HeroImage';
import SearchBar from '../elements/SearchBar/SearchBar';
import FourColGrid from '../elements/FourColGrid/FourColGrid';
import MovieThumb from '../elements/MovieThumb/MovieThumb';
import LoadMoreBtn from '../elements/LoadMoreBtn/LoadMoreBtn';
import Spinner from '../elements/Spinner/Spinner';
import './Home.css';

class Home extends Component {
  state = {
    movies: [],
    heroImage: null,
    loading: false,
    currentPage: 0,
    totalPages: 0,
    searchTerm: ''
  }

  // lifeCycle method from react
  // https://developers.themoviedb.org/3/movies/get-popular-movies
  componentDidMount() {
    if (localStorage.getItem('HomeState')) {
      const state = JSON.parse(localStorage.getItem('HomeState'));
      this.setState({...state});
    } else {
      this.setState( { loading: true })
      this.fetchItems(this.popularEP(false)(""));
    }
  }

  // searchItems + loadMoreItems methods
  // below is same as :
  // curriedEndpoint = type => { return loadMore => { return searchTerm => {}}}
  curriedEndpoint = type => loadMore => searchTerm => 
    `${API_URL}${type}?api_key=${API_KEY}&language=en-US&page=
      ${loadMore && this.state.currentPage + 1}&query=${searchTerm}`;
  
  searchEP = this.curriedEndpoint("search/movie");
  popularEP = this.curriedEndpoint("movie/popular");

  // curriedEndpoint = (type, loadMore, searchTerm) => {
  //   return `${API_URL}${type}?api_key=${API_KEY}&language=en-US&page=
  //     ${loadMore && this.state.currentPage + 1}&query=${searchTerm}`;
  // }

  // searchItems + loadMoreItems methods
  updateItems = (loadMore, searchTerm) => {
    this.setState({
      movies: loadMore ? [...this.state.movies] : [],
      loading: true,
      searchTerm: loadMore ? this.state.searchTerm : searchTerm
    }, () => {
      this.fetchItems(
        !this.state.searchTerm 
        ? this.popularEP(loadMore)("") // ("") is searchTerm part at curriedEndpoint
        : this.searchEP(loadMore)(this.state.searchTerm)
        // ? this.curriedEndpoint("movie/popular", loadMore, "")
        // : this.curriedEndpoint("search/movie", loadMore, this.state.searchTerm)
      )
    })
  }

  // searchItems = (searchTerm) => {
  //   let endpoint = '';
  //   this.setState({
  //     movies: [],
  //     loading: true,
  //     searchTerm
  //   })

  //   if (searchTerm === "") {
  //     endpoint = `${API_URL}movie/popular?api_key=${API_KEY}&language=en-US&page=1`;
  //   } else {
  //     endpoint = `${API_URL}search/movie?api_key=${API_KEY}&language=en-US&query=${searchTerm}`;
  //   }

  //   this.fetchItems(endpoint);
  // }

  // loadMoreItems = () => {
  //   const { searchTerm, currentPage } = this.state;

  //   let endpoint = '';
  //   this.setState({ loading: true });

  //   if (searchTerm === "") {
  //     endpoint = `${API_URL}movie/popular?api_key=${API_KEY}&language=en-US&page=${currentPage + 1}`;
  //   } else {
  //     endpoint = `${API_URL}search/movie?api_key=${API_KEY}&language=en-US&query=${searchTerm}&page=${currentPage + 1}`;
  //   }
  //   this.fetchItems(endpoint);
  // }

  fetchItems = async endpoint => {
    const { movies, heroImage, searchTerm } = this.state;
    // await data to be fetched from the endpoint, 
    // then await data data to be converted into json file 
    const result = await (await fetch(endpoint)).json(); 
    try {
      this.setState({
        // want to append new movies to the old movies
        // ...this <- copy the old movies array
        // spread syntac (es6)
        movies: [...movies, ...result.results],
        heroImage: heroImage || result.results[0],
        loading: false,
        currentPage: result.page,
        totalPages: result.total_pages
      }, () => {
        if (searchTerm === "") {
          localStorage.setItem('HomeState', JSON.stringify(this.state));
        }
      });
    } catch (e) {
      console.log("There was an error: ", e);
    }
  }

  // fetchItems = (endpoint) => {
  //   const { movies, heroImage, searchTerm } = this.state;

  //   fetch(endpoint)
  //     .then(result => result.json())
  //     .then(result => {
  //       this.setState({
  //         movies: [...movies, ...result.results],
  //         heroImage: heroImage || result.results[0],
  //         loading: false,
  //         currentPage: result.page,
  //         totalPages: result.total_pages
  //       }, () => {
  //         if (searchTerm === "") {
  //           localStorage.setItem('HomeState', JSON.stringify(this.state));
  //         }
  //       });
  //     })
  //     .catch(error => console.error('Error', error))
  // }

  render() {
    // ES6 destructuring the state
    const { movies, heroImage, loading, currentPage, totalPages, searchTerm } = this.state;
    // const { prop1, prop2 } = this.props; // <- if we are sending a props to stateful class component
    return (
      <div className="rmdb-home">
      {heroImage && !searchTerm ? // if heroImage exists, do div, otherwise return null
        <div>
          <HeroImage
            image={`${IMAGE_BASE_URL}${BACKDROP_SIZE}${heroImage.backdrop_path}`}
            title={heroImage.original_title}
            text={heroImage.overview} 
          />
        </div> : null }
        <SearchBar callback={this.updateItems} />
        <div className="rmdb-home-grid">
          <FourColGrid 
            header={searchTerm ? 'Search Result' : 'Popular Movies'}
            loading={loading}
          >
            {movies.map( (element, i) => {
              return <MovieThumb  
                        key={i}
                        clickable={true}
                        image={element.poster_path ? `${IMAGE_BASE_URL}${POSTER_SIZE}${element.poster_path}` : './images/no_image.jpg'}
                        movieId={element.id}
                        movieName={element.original_title}
                        />
            })}
          </FourColGrid>
          {loading ? <Spinner /> : null}
          {(currentPage < totalPages && !loading) ?
              <LoadMoreBtn text="Load More" onClick={this.updateItems} />
              : 
              null 
          }
        </div>
      </div>
    )
  }
}

export default Home;
